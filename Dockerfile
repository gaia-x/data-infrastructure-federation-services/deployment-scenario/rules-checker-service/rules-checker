FROM node:alpine

WORKDIR /usr/src/app

COPY package*.json ./
COPY tsconfig.json ./

COPY . .
RUN npm install
RUN npm run build

EXPOSE 443

CMD ["node", "./dist/index.js"]
