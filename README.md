# rules checker

# Getting started
```
git clone https://gitlab.com/gaia-x/data-infrastructure-federation-services/deployment-scenario/rules-checker-service/rules-checker.git
npm install
```

## Description API

This API will be used to verify whether you are compliant with the gaia-x standards and whether or not to label your gaia-x compliant services.

### Global variables

provide some environnements variables : 
- DID = DID used to issue VCs : did:web:abc-federation.gaia-x.community 
- OPA_SERVICE_HOST : ip address (or name) of the OPA components to execute rego rules 
- OPA_SERVICE_PORT : port of the OPA components to execute rego rules 
- VC_ISSUER_SERVICE_HOST : ip address (or name) of the issuer component 
- VC_ISSUER_SERVICE_PORT : port of the issuer component 
- VC_URI = a DID with a %s for the generated id like this sample : did:web:ruleschecker.abc-federation.gaia-x.community/vc/%s 
- VCS = directory to store VC files like /var/vcs/ 

to start the component : ./start.sh 

## APIs

HTTP POST /rules/{rulesGroupe}/{ruleName} and a json document

Allowed values (but not exhaustive) 
| ruleGroup | ruleName |
| ------ | ------ |
|    compliance    |  complianceParticipant      |
|    compliance    |   complianceServiceOffering     |
|    labelling    |   label     |

This service will call OPA Server with theses values, it's allowed to add rules and call them with this service without updating this service. 

This call return a VC signed.

HTTP GET /vc/{id} to retreive the VC signed already provided 

HTTP GET /vc/{id}/.well-known/did.json to retreive the VC signed 

HTTP GET /countVC/ to get the VC count 

### test API
```
curl -X POST https://ruleschecker.abc-federation.gaia-x.community/rules/complianceParticipant -H "Content-Type: application/json" -d @tests/test-ok.json

```

## Labelling (WIP)
The awarding of a label to your service will depend on the certifications it contains and the criteria you meet.

#### list of criteria
https://gitlab.com/gaia-x/policy-rules-committee/label-document/-/blob/master/docs/policy_rules_labeling_document.md#contractual-governance

#### No label:
Your service does not contain any certification and you do not comply with Gaia-x standards.

#### Label 1:
C32, C33, C34, C35, C36, C37, C38, C39, C40, C41, C42, C43, C44, C45, C46, C47, C48, C49, C50, C51 in self-declaration  
or / and  
C19, C20, C21, C22, C23, C24, C25, C26, C27, C28, C29, C30, C31, C54​ in self-declaration  
or / and  
C19, C20, C21, C22, C23, C24, C25, C26, C27, C28, C29, C30, C31 in self-declaration  
or / and  
C52, C53​ in self-declaration  
or / and  
C1, C2, C3, C4, C5, C6, C7, C8, C9, C10, C11, C12, C13, C54, C55, C56, C57, C58, C59, C60, C61 in self-declaration.  

#### label 2&3:
C32, C33, C34, C35, C36, C37, C38, C39, C40, C41, C42, C43, C44, C45, C46, C47, C48, C49, C50, C51   
or / and  
C19, C20, C21, C22, C23, C24, C25, C26, C27, C28, C29, C30, C31, C54​  
or / and  
C19, C20, C21, C22, C23, C24, C25, C26, C27, C28, C29, C30, C31  
or / and  
C52, C53​  
or / and  
C1, C2, C3, C4, C5, C6, C7, C8, C9, C10, C11, C12, C13, C54, C55, C56, C57, C58, C59, C60, C61.  


