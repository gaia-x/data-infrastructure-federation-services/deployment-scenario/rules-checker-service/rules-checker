const express = require('express');
const fs = require('fs');
const axios = require('axios');
import { v4 as uuidv4 } from 'uuid';
const jose2 = require('jose')
const NodeRSA = require('node-rsa');
const  btoa = require('btoa');

let config = [];

const app = express();
app.use(express.json());

async function rule(typeRule,rule,vc) {
  try {
    let urlType = config['POLICY'].replace('%t', typeRule);
    let urlRule = urlType.replace ('%s' , rule);
    console.log ("urlrule : " + urlRule);
    let responseRules = await axios.post(urlRule,{"input":vc}, {headers: {'Content-Type': 'application/json'}},);
    console.log ("ResponseRule : " + JSON.stringify(responseRules));
    console.log ("----- rule : " + rule + "-------");
    console.log (responseRules.data);
    if (responseRules.data.result.valid=="true") {
      //responseRules.data.result.VC.issuanceDate = new Date().toISOString();
      responseRules.data.result.VC.expirationDate = new Date().toISOString();
      responseRules.data.result.VC['@type'][0] = 'VerifiableCredential';
      responseRules.data.result.VC['@type'][1] = rule ;
      responseRules.data.result.VC.issuer = config['DID'];
      responseRules.data.result.VC.evidence.verifier = config['DID'];
      let id = uuidv4();
      responseRules.data.result.VC['@id'] =  config['VC_URI'].replace('%s',id);
      fs.writeFileSync(config['VCS'] + id + '.json', JSON.stringify(responseRules.data.result.VC, null, ' '));
      console.log (responseRules.data.result.VC);
      let responseSign = await axios.put(config['SIGN'], responseRules.data.result.VC, {headers: {'Content-Type': 'application/json'}},);
      console.log ("sign return OK" + responseSign.data);
      return { "statut" : "true" , "VC" : responseSign.data};
    } else {
      console.log (responseRules.data);

      return { "statut" : "false" , "hasInvalidRules" : JSON.stringify(responseRules.data.result.lstNonValid )};
    }
  } catch (error) {
  console.log (error);
  return { "statut" : "false" , "error" : error };
}
}

app.get('/vc/:vc/', (req, res) => {
  res.json (JSON.parse(fs.readFileSync ('/var/vcs/' + req.params.vc + '.json')));
  res.end();
})

app.get('/countVC/', (req, res) => {
  fs.readdir(config['VCS'] , (err, files) => {
    res.json ( { "statut": "true" , "count" : files.length}  );
    res.end();
  });
})

app.get('/vc/:vc/.well-known/did.json', (req, res) => {
  res.json (JSON.parse(fs.readFileSync ('/var/vcs/' + req.params.vc + '.json')));
  res.end();
})

app.post('/rules/:typeRule/:rule/', (req, res) => {
  rule (req.params.typeRule,req.params.rule, req.body).then(result => {  
  res.json (result);
  res.end();
  })
})

// gere les erreurs pour retourner un json avec statut:false
app.use((error, request, response, next) => {
  if (error instanceof SyntaxError)
    response.status(500).send({"statut": "false" , "message" : JSON.stringify (error)});
  else 
    next();
});

// logue les erreurs sur la console.
app.use((error, request, response, next) => {
  if (error instanceof SyntaxError)
    console.error(error) // or using any fancy logging library
  else 
    next(error);
});

// check the pod have access to the private key and a storage for VCs, if not exit !
function checkConfig() {
  if (!fs.existsSync(config['VCS'])) {
    console.error ("not access to " + config['VCS'] + ' to store VC');
    process.exit(1);
  }
}
function loadConfig() {
  config['DID'] = process.env.DID;
  config['POLICY'] = 'https://policyrules.abc-federation.gaia-x.community/v1/data/rules/%t/%s/valid';
  config['SIGN'] = 'http://' + process.env.VC_ISSUER_SERVICE_HOST + ':' + process.env.VC_ISSUER_SERVICE_PORT + '/api/v0.9/sign';
  config['VC_URI'] = process.env.VC_URI;
  //config['KEY'] = process.env.PRIVATE_KEY_PATH;
  config['VCS'] = process.env.VCS;
}

async function main () {
try {
    loadConfig();
    console.log (config);
    checkConfig();
    fs.writeFileSync(config['VCS'] + 'healthcheck.json', JSON.stringify({ "status" : "OK"}));
    app.listen(8080, () => {
      console.log('HTTPS Server running on port 8080 v0.009');
    })
  } catch (error) {
    console.log (error);
  }
}

main();
